//
//  ChatService.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 22/06/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import Foundation
import Scaledrone

class ChatService{
    
    private let scaledrone: Scaledrone
    private let messageCallback: (Message) -> Void
    
    private var room: ScaledroneRoom?
    
    init(member: Member, onReceivedMessage: @escaping(Message)->Void) {
        self.messageCallback = onReceivedMessage
        self.scaledrone = Scaledrone(
            channelID: "28eZPqAisDy5ggra",
            data: member.toJSON)
        scaledrone.delegate = self
    }
    
    func connect(){
        scaledrone.connect()
    }
    
    func sendMessage(_ message: String) {
        room?.publish(message: message)
    }

}

extension ChatService: ScaledroneRoomDelegate{
    func scaledroneRoomDidConnect(room: ScaledroneRoom, error: Error?) {
        print("Connected to room")
    }
    
    func scaledroneRoomDidReceiveMessage(room: ScaledroneRoom, message: Any, member: ScaledroneMember?) {
        guard let text = message as? String, let memberdata = member?.clientData, let member = Member(fromJSON: memberdata)
            else {
                print("couldnt parse data")
                return
        }
        
        let message = Message(member: member, text: text, messageId: UUID().uuidString)
        messageCallback(message)
    }
    
    
}

extension ChatService: ScaledroneDelegate{
    func scaledroneDidConnect(scaledrone: Scaledrone, error: Error?) {
        print("Connected to Scaledrone")
        room = scaledrone.subscribe(roomName: "observable-room")
        room?.delegate = self
    }
    
    func scaledroneDidReceiveError(scaledrone: Scaledrone, error: Error?) {
        print("Scaledrone error", error ?? "")
    }
    
    func scaledroneDidDisconnect(scaledrone: Scaledrone, error: Error?) {
        print("Scaledrone error", error ?? "")
    }
    
    
}
