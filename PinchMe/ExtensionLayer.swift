//
//  ExtensionLayer.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 24/06/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import Foundation
import UIKit

extension CALayer{
    func shadowView(){
        shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        shadowOffset = CGSize(width: 2.0, height: 2.0)
        shadowOpacity = 1.0
        opacity = 0.5
        shadowRadius = 0.0
        masksToBounds = false
    }
    
    func shadowImageButton(){
        shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        shadowOffset = CGSize(width: 2.0, height: 2.0)
        shadowOpacity = 1.0
        shadowRadius = 0.0
        masksToBounds = false
    }
    
    func shadowButton() {
        shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        shadowOffset = CGSize(width: 2.0, height: 3.0)
        shadowOpacity = 1.0
        shadowRadius = 0.0
        masksToBounds = false
    }
    
    func shadowIfDone(){
        shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.25).cgColor
        shadowOffset = CGSize(width: 2.0, height: 2.0)
//        shadowOpacity = 1.0
//        opacity = 0.5
        shadowRadius = 0.0
        masksToBounds = false
    }
}
