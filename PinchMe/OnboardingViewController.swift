//
//  OnboardingViewController.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 04/06/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class OnboardingViewController: UIViewController {

    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func handleStartButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let mainVC = storyboard.instantiateViewController(withIdentifier: "tabBar")
        mainVC.modalPresentationStyle = .fullScreen
        
        self.present(mainVC, animated: true, completion: nil)
    }
    
}
