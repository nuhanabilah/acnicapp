//
//  HomeCameraViewController.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 27/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit
import AVFoundation

class HomeCameraViewController: UIViewController {
    
    @IBOutlet weak var faceView: UIView!
    @IBOutlet weak var foreheadView: UIView!
    @IBOutlet weak var rigthCheekView: UIView!
    @IBOutlet weak var leftCheekView: UIView!
    @IBOutlet weak var daguView: UIView!
    @IBOutlet weak var leherView: UIView!
    @IBOutlet weak var rahangKananView: UIView!
    @IBOutlet weak var rahangKiriView: UIView!
    
    @IBOutlet weak var faceButton: UIButton!
    @IBOutlet weak var forheadButton: UIButton!
    @IBOutlet weak var rightCheekButton: UIButton!
    @IBOutlet weak var leftCheekButton: UIButton!
    @IBOutlet weak var daguButton: UIButton!
    @IBOutlet weak var leherButton: UIButton!
    @IBOutlet weak var rahangKananButton: UIButton!
    @IBOutlet weak var rahangKiriButton: UIButton!
    
    @IBOutlet weak var sendButton: UIButton!
    
    var brigeData = UserPhotoData()
    var statusPart: Part?
    var cekLogin: Bool = UserDefaults.standard.bool(forKey: "cekMasuk1")

    override func viewDidLoad() {
        super.viewDidLoad()
        
        faceView.layer.shadowView()
        faceButton.layer.shadowImageButton()
        
        foreheadView.layer.shadowView()
        forheadButton.layer.shadowImageButton()
        
        rigthCheekView.layer.shadowView()
        rightCheekButton.layer.shadowImageButton()
        
        leftCheekView.layer.shadowView()
        leftCheekButton.layer.shadowImageButton()
        
        daguView.layer.shadowView()
        daguButton.layer.shadowImageButton()
        
        leherView.layer.shadowView()
        leherButton.layer.shadowImageButton()
        
        rahangKananView.layer.shadowView()
        rahangKananButton.layer.shadowImageButton()
        
        rahangKiriView.layer.shadowView()
        rahangKiriButton.layer.shadowImageButton()
        
        sendButton.layer.shadowButton()
        
        self.navigationController?.navigationBar.prefersLargeTitles = false

    }
    
    @IBAction func takeFacePhoto(_ sender: Any) {
        statusPart = .face
        
        let controller = CustomCameraViewController()
        controller.statusPart = statusPart
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func takeForeheadPhoto(_ sender: Any) {
        statusPart = .forehead
        
        let controller = CustomCameraViewController()
        controller.statusPart = statusPart
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func takeRightCheekPhoto(_ sender: Any) {
        statusPart = .rightCheek
        
        let controller = CustomCameraViewController()
        controller.statusPart = statusPart
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func takeLeftCheekPhoto(_ sender: Any) {
        statusPart = .leftCheek
        
        let controller = CustomCameraViewController()
        controller.statusPart = statusPart
        self.present(controller, animated: true, completion: nil)
    }
    
    @IBAction func handleSendData(_ sender: Any) {
        if  cekLogin == true {
            alertSukses(title: "Data anda telah terkirim", message: "Selamat data anda telah terkirim")}
        else {
        alertDataDiri(title: "Data Diri", message: "Untuk memudahkan konsultasi, silakan mengisi data diri kamu terlebih dahulu.")
        }
        
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let vc = storyboard.instantiateViewController(withIdentifier: "IdentifyViewController") as! IdentifyViewController
//
//        vc.stepOneCheck = true
//        vc.stepTwoCheck = true
//
//        self.navigationController!.pushViewController(vc, animated: true)
    }
    
    func alertDataDiri(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(ACTION) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "halamanLogin") as! halamanLogin
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
     func alertSukses(title: String, message: String){
            let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
            
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(ACTION) in
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let vc = storyboard.instantiateViewController(withIdentifier: "IdentifyViewController") as! IdentifyViewController
                self.navigationController?.pushViewController(vc, animated: true)
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    



