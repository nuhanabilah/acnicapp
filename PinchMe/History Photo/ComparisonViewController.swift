//
//  ComparisonViewController.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 28/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class ComparisonViewController: UIViewController {

    @IBOutlet weak var seeAllButton: UIButton!
    @IBOutlet weak var firstImageView: UIImageView!
    @IBOutlet weak var lastImageView: UIImageView!
    @IBOutlet weak var firstDateLabel: UILabel!
    @IBOutlet weak var lastDateLabel: UILabel!
    
    var statusPart: Part?
    var bridgeData = UserPhotoData()
    var arrayOfPict: [UserStruct] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(statusPart)
        
        arrayOfPict = bridgeData.arrayOfUserPhoto
       
//        let imageData = UserDefaults.standard.object(forKey: "face")
//        var image = UIImage(data: imageData as! Data)
        firstImageView.image = arrayOfPict[2].photo
        firstDateLabel.text = arrayOfPict[2].dateTaken
               
//        let imageData2 = UserDefaults.standard.object(forKey: "forehead")
//        print(imageData2)
//        var image2 = UIImage(data: imageData2 as! Data)
        lastImageView.image = arrayOfPict[4].photo
        lastDateLabel.text = arrayOfPict[4].dateTaken
    }
    @IBAction func handleSeeAll(_ sender: Any) {
        print("tapped")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SeeAllPhotoViewController") as! SeeAllPhotoViewController
//        self.navigationController?.pushViewController(vc, animated: true)
        self.present(vc, animated: true, completion: nil)
    }
    
}
