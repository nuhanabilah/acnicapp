//
//  CustomCameraViewController.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 27/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit
import AVFoundation

class CustomCameraViewController: UIViewController, AVCapturePhotoCaptureDelegate {
    
    var photoOutput = AVCapturePhotoOutput()
    var statusPart: Part?
    var bridgeData = UserPhotoData()
    var arrayOfPhoto:[UserStruct] = []
    
    var arrayOfFace:[UIImage] = []
    
    lazy private var backButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(systemName: "xmark"), for: .normal)
        button.addTarget(self, action: #selector(handleDismiss), for: .touchUpInside)
        button.tintColor = .white
        return button
    }()
    
    lazy private var takePhotoButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "capture_photo")?.withRenderingMode(.alwaysOriginal), for: .normal)
        button.addTarget(self, action: #selector(handleTakePhoto), for: .touchUpInside)
        return button
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        arrayOfPhoto = bridgeData.arrayOfUserPhoto
        openCamera()
    }
    
    func openCamera() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .authorized:
            self.setupCaptureSession()
            
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video){ (granted) in
                if granted{
                    print("The user has granted to access the camera")
                    DispatchQueue.main.async {
                        self.setupCaptureSession()
                    }
                } else {
                    print("User has not granted to access the camera")
                    self.handleDismiss()
                }
            }
        
        case .denied:
            print("the user has denied previously to access the camera.")
            self.handleDismiss()
            
        case .restricted:
            print("the user can't give camera access due to some restriction.")
            self.handleDismiss()
            
        default:
            print("something has wrong due to we can't access the camera.")
            self.handleDismiss()
        }
    }
    
    func setupCaptureSession() {
        let captureSession = AVCaptureSession()
        
        if let captureDevice = AVCaptureDevice.default(for: AVMediaType.video){
            do{
                let input = try AVCaptureDeviceInput(device: captureDevice)
                if captureSession.canAddInput(input){
                    captureSession.addInput(input)
                }
            }catch let error {
                print("failed to set input device with error: \(error)")
            }
            
            if captureSession.canAddOutput(photoOutput){
                captureSession.addOutput(photoOutput)
            }
            
            let cameraLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            cameraLayer.frame = self.view.frame
            cameraLayer.videoGravity = .resizeAspectFill
            self.view.layer.addSublayer(cameraLayer)
            
            captureSession.startRunning()
            self.setupUI()
        }
    }
    
    func setupUI() {
        
        view.addSubview(backButton)
        view.addSubview(takePhotoButton)
        
        takePhotoButton.makeConstraints(top: nil, left: nil, right: nil, bottom: view.safeAreaLayoutGuide.bottomAnchor, topMargin: 0, leftMargin: 0, rightMargin: 0, bottomMargin: 15, width: 80, height: 80)
        takePhotoButton.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        backButton.makeConstraints(top: view.safeAreaLayoutGuide.topAnchor, left: nil, right: view.rightAnchor, bottom: nil, topMargin: 15, leftMargin: 0, rightMargin: 10, bottomMargin: 0, width: 50, height: 50)
        
        print("SETUP UI")
    }
    
    @objc private func handleDismiss(){
        DispatchQueue.main.async {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc private func handleTakePhoto(){
        let photoSetting = AVCapturePhotoSettings()
        if let photoPreviewType = photoSetting.availablePreviewPhotoPixelFormatTypes.first{
            photoSetting.previewPhotoFormat = [kCVPixelBufferPixelFormatTypeKey as String: photoPreviewType]
            photoOutput.capturePhoto(with: photoSetting, delegate: self)
        }
        print("handle take photo")
    }
    
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard let imageData = photo.fileDataRepresentation() else {
            return
        }
        
        let previewImage = UIImage(data: imageData)
//        var imageTemp:[UIImage] = []
//        
//        if statusPart == .face{
//            var imageArray: [UIImage] = []
//
//            if let imageTemp = UserDefaults.standard.array(forKey: "face"){
//                imageArray = imageTemp as! [UIImage]
//                imageArray.append(imageTemp)
//            }
//            imageArray.append(previewImage!)
//            UserDefaults.standard.set(imageArray, forKey: "face")
//            
//            if imageArray.count==0{
//                UserDefaults.standard.set(imageArray, forKey: "face")
//            } else {
//                imageTemp = UserDefaults.standard.object(forKey: "face") as! [UIImage]
//                imageArray.append(contentsOf: imageTemp)
//                UserDefaults.standard.set(imageArray, forKey: "face")
//
//                for index in 0..<imageArray.count{
//                    print("image Array")
//                    print(imageArray[index])
//                }
//            }
//
//
//
//        } else if statusPart == .forehead{
//            print("Photo output: ")
//            print(statusPart)
//            print(imageData)
//            print("------------")
//            UserDefaults.standard.set(imageData, forKey: "forehead")
//        } else if statusPart == .rightCheek{
//            UserDefaults.standard.set(imageData, forKey: "rightCheek")
//        } else if statusPart == .leftCheek{
//            UserDefaults.standard.set(imageData, forKey: "leftCheek")
//        }
        
        let photoPreviewContainer = PhotoPreviewView(frame: self.view.frame)
        photoPreviewContainer.photoImageView.image = previewImage
        photoPreviewContainer.tempData = imageData
        photoPreviewContainer.statusPart = statusPart
        self.view.addSubview(photoPreviewContainer)
    
    }

}

extension UIView {
    
    func makeConstraints(top: NSLayoutYAxisAnchor?, left: NSLayoutXAxisAnchor?, right: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, topMargin: CGFloat, leftMargin: CGFloat, rightMargin: CGFloat, bottomMargin: CGFloat, width: CGFloat, height: CGFloat) {
        
        self.translatesAutoresizingMaskIntoConstraints = false
        if let top = top {
            self.topAnchor.constraint(equalTo: top, constant: topMargin).isActive = true
        }
        
        if let left = left {
            self.leftAnchor.constraint(equalTo: left, constant: leftMargin).isActive = true
        }
        
        if let right = right {
            self.rightAnchor.constraint(equalTo: right, constant: -rightMargin).isActive = true
        }
        
        if let bottom = bottom {
            self.bottomAnchor.constraint(equalTo: bottom, constant: -bottomMargin).isActive = true
        }
        
        if width != 0 {
            self.widthAnchor.constraint(equalToConstant: width).isActive = true
        }
        
        if height != 0 {
            self.heightAnchor.constraint(equalToConstant: height).isActive = true
        }
    }
    
    func addSubviews(_ views: UIView...) {
        views.forEach{ addSubview($0) }
    }
}
