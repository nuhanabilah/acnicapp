//
//  HistoryViewController.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 29/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource{
    
    let sheetVc = SheetViewController()
    var heightConstraint:NSLayoutConstraint!
    public fileprivate(set) var blackOverlay: UIControl = UIControl()
    
    let brideData = UserPhotoData()
    var arrayOfData: [UserStruct] = []

    @IBOutlet weak var historyCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
//        navigationController?.setNavigationBarHidden(true, animated: true)
        
        let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 15
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: 374, height: 110)
        historyCollectionView.collectionViewLayout = layout
        
//        arrayOfData = UserDefaults.standard.object(forKey: "arrayFace") as! [UserStruct]
//        let decodedTeams = NSKeyedUnarchiver.unarchiveObject(with: arrayOfData) as! [UserStruct]
//        historyCollectionView.reloadData()
//        print(brideData.arrayOfUserPhoto)
//        print(arrayOfData)
        
        arrayOfData = brideData.arrayOfUserPhoto

        
        print("TEST")
    }
    
    @objc func showTransparent(_ sender: Any) {
        sheetVc.view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(sheetVc.view)
       
        NSLayoutConstraint.activate([
            sheetVc.view.topAnchor.constraint(equalTo: self.view.topAnchor),
            sheetVc.view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            sheetVc.view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            sheetVc.view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
        ])
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HistoryCollectionViewCell", for: indexPath) as! HistoryCollectionViewCell
        cell.titleLabel.text = arrayOfData[indexPath.item].part.rawValue
        let part = arrayOfData[indexPath.item].part.rawValue
        switch part {
        case "Muka":
            cell.illustationImageView.image = UIImage(named: "circle_wajah")
        case "Dahi":
            cell.illustationImageView.image = UIImage(named: "circle_dahi")
        case "Pipi Kanan" :
            cell.illustationImageView.image = UIImage(named: "circle_pipi_kanan_samping")
        default:
            break
        }
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        if arrayOfData[indexPath.item].part == .face {
//            UserDefaults.standard.set(0, forKey: "a")
//        }
//
        showTransparent(self)
        
    }
    
}
