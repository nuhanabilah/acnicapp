//
//  Question1ViewController.swift
//  PinchMe
//
//  Created by Faisal on 25/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class Question1ViewController: UIViewController {

    @IBOutlet weak var LabelQuestion1: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.isHidden = false
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
//
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(animated)
//        navigationController?.setNavigationBarHidden(false, animated: animated)
//    }
    @IBAction func unwindToScreenBefore (_ unwindSegue: UIStoryboardSegue) {

    }

    @IBAction func nextButton(_ sender: UIButton) {
        performSegue(withIdentifier: "toQuestion2", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.destination is Question2ViewController{
            if let passData = segue.destination as? Question2ViewController{
                passData.Question1 = LabelQuestion1.text!
            }
        }
    }

}
