//
//  Consultation.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 23/06/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import Foundation
import UIKit

struct LogConsultation {
    var consultation: String
    var dateConsultation: Date
}

class Consultation{
    var arrayOfConsultation:[LogConsultation] = [LogConsultation(consultation: "Konsulation 1", dateConsultation: Date())]
}
