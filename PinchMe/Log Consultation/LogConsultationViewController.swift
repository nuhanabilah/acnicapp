//
//  LogConsultationViewController.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 23/06/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class LogConsultationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var logConsulTableView: UITableView!
    
    var bridge = Consultation()
    var array:[LogConsultation] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        array = bridge.arrayOfConsultation
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return array.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LogConsultationTableViewCell") as! LogConsultationTableViewCell
        cell.logConsulLabel.text = array[indexPath.item].consultation
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = RoomChatViewController()
        navigationController?.pushViewController(vc, animated: true)
    }

}
