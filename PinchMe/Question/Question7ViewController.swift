//
//  Question7ViewController.swift
//  PinchMe
//
//  Created by Faisal on 25/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class Question7ViewController: UIViewController {

    @IBOutlet weak var LabelQuestion7: UITextField!
    var Question1 = ""
    var Question2 = ""
    var Question3 = ""
    var Question4 = ""
    var Question5 = ""
    var Question6 = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        print(Question1)
        print(Question2)
        print(Question3)
        print(Question4)
        print(Question5)
        print(Question6)

        // Do any additional setup after loading the view.
        
        UserDefaults.standard.set(Question1, forKey: "Question1")
        UserDefaults.standard.set(Question2, forKey: "Question2")
        UserDefaults.standard.set(Question3, forKey: "Question3")
        UserDefaults.standard.set(Question4, forKey: "Question4")
        UserDefaults.standard.set(Question5, forKey: "Question5")
        UserDefaults.standard.set(Question6, forKey: "Question6")
        UserDefaults.standard.set(LabelQuestion7.text, forKey: "Question7")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.destination is HasilWawancaraViewController{
        if let passData = segue.destination as? HasilWawancaraViewController{
            passData.Question1 = Question1
            passData.Question2 = Question2
            passData.Question3 = Question3
            passData.Question4 = Question4
            passData.Question5 = Question5
            passData.Question6 = Question6
            passData.Question7 = LabelQuestion7.text!
        }
    }
    }
    
    
    @IBAction func nextButton(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "IdentifyViewController") as! IdentifyViewController
        
        vc.stepOneCheck = true
        
        self.navigationController!.pushViewController(vc, animated: true)
    }

}
