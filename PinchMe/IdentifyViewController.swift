//
//  IdentifyViewController.swift
//  PinchMe
//
//  Created by Faisal on 27/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class IdentifyViewController: UIViewController {
    @IBOutlet weak var wawancaraAnamnesaLabel: UILabel!
    @IBOutlet weak var fotoFisikLabel: UILabel!
    @IBOutlet weak var signImageView: UIImageView!
    @IBOutlet weak var secondSignImageView: UIImageView!
    @IBOutlet weak var gifImageView: UIImageView!
    @IBOutlet weak var girlImageView: UIImageView!
    @IBOutlet weak var boyImageView: UIImageView!
    
    var stepOneCheck:Bool = false
    var stepTwoCheck:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        wawancaraAnamnesaLabel.font = UIFont.boldSystemFont(ofSize: wawancaraAnamnesaLabel.font.pointSize)
        fotoFisikLabel.font = UIFont.boldSystemFont(ofSize: fotoFisikLabel.font.pointSize)
    
        girlImageView.layer.shadowImageButton()
        boyImageView.layer.shadowImageButton()
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: true)
        navigationItem.hidesBackButton = true
        self.tabBarController?.tabBar.isHidden = false
        
        if stepOneCheck == false{
            signImageView.image = UIImage(named: "tanda_seru")
        } else {
            signImageView.image = UIImage(named: "centang")
        }
        
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    
    @IBAction func nextButton(_ sender: UIButton) {
        performSegue(withIdentifier: "toInterview", sender: self)
    }
    
    @IBAction func secondStepButton(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "HomeCameraViewController") as! HomeCameraViewController
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
   
    @IBAction func profileButton(_ sender: Any) {
        performSegue(withIdentifier: "keProfile", sender: self)
    }
    

}
extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}
