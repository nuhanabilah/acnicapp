//
//  TakeAPictureViewController.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 22/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class TakeAPictureViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var faceButton: UIButton!
    @IBOutlet weak var foreheadButton: UIButton!
    
    @IBOutlet weak var imageResultTemp: UIImageView!
    @IBOutlet weak var imageResultTemp2: UIImageView!
    
    var imageTemp: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func faceButtonHandler(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = false

            self.present(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func foreheadButtonHandler(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera){
            let imagePicker2 = UIImagePickerController()
            imagePicker2.delegate = self
            imagePicker2.sourceType = UIImagePickerController.SourceType.camera
            imagePicker2.allowsEditing = false
            
            self.present(imagePicker2, animated: true, completion: nil)
        }
    
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            imageResultTemp.image = pickedImage
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
}
