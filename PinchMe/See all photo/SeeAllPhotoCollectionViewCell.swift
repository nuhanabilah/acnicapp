//
//  SeeAllPhotoCollectionViewCell.swift
//  PinchMe
//
//  Created by Ulinnuha Nabilah on 02/06/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class SeeAllPhotoCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var dateTakenLabel: UILabel!
    
}
