//
//  halamanSignup.swift
//  PinchMe
//
//  Created by Kevin Heryanto on 27/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class halamanSignup: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var inputNama: UITextField!
    @IBOutlet weak var inputtelepon: UITextField!
    @IBOutlet weak var inputEmail: UITextField!
    @IBOutlet weak var birthPicker: UITextField!
    @IBOutlet weak var genre: UITextField!
    @IBOutlet weak var signUpButton: UIButton!
    //    var nama:String?
//    var nomer:String?
//    var email:String?
//    var tanggal:String?
    
    let Genrer = ["Laki-laki",
                  "Perempuan"]
    var selectedGenre: String?
    let datePicker = UIDatePicker()
    var cekLogin1: Bool = Bool()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        signUpButton.layer.shadowButton()
        
        createPicker()
        createViewPicker()
        createtoolbar()
        hideKeyboardWhenTappedAround()
    }
    
    
    // JenisKelaminViewPicker
    
    func createViewPicker() {
     let pickerKelamin = UIPickerView()
        pickerKelamin.delegate = self
        pickerKelamin.dataSource = self
        genre.inputView = pickerKelamin
      }
        func numberOfComponents(in pickerView: UIPickerView) -> Int{
            return 1
        }
    func pickerView(_  pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return Genrer.count
        }
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return Genrer[row]
        }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            selectedGenre = Genrer[row]
            genre.text = selectedGenre
        }
    
    func createtoolbar() {
           let tulbar = UIToolbar()
           tulbar.sizeToFit()
           
        let doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(dismissKeyboard))
           tulbar.setItems([doneBtn], animated: false)
           tulbar.isUserInteractionEnabled = true
           
           genre.inputAccessoryView = tulbar
//        genre.inputView = pickerKelamin
       }
       
      
        
    
    
    
    // DatePicker
    
    func createPicker() {
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        
        let doneBtn = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(pencetDone))
        toolbar.setItems([doneBtn], animated: true )
        birthPicker.inputAccessoryView = toolbar
        
        birthPicker.inputView = datePicker
        
        datePicker.datePickerMode = .date
    }

    @objc func pencetDone() {
        let format = DateFormatter()
        format.dateStyle = .medium
        format.timeStyle = .none
        
        
        birthPicker.text = format.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    
    
    // RegisterButton
    @IBAction func register(_ sender: AnyObject) {
        let nama = inputNama.text;
        let nomer = inputtelepon.text;
        let email = inputEmail.text;
        let tanggal = birthPicker.text;
        let jeniskelamin = genre.text;
        
//        print(nama, nomer, email, tanggal, jeniskelamin)
        
        if (nama!.isEmpty || nomer!.isEmpty || email!.isEmpty || tanggal!.isEmpty || jeniskelamin!.isEmpty) {
            
            displayAlertMessage(userMessage: "Semua data harus diisi")
            return;
        }else{
            alertSukses(title: "Selamat", message: "Data anda berhasil dikirim")
            UserDefaults.standard.set(nama, forKey: "namaUser")
            UserDefaults.standard.set(nomer, forKey: "nomorUser")
            UserDefaults.standard.set(email, forKey: "emailUser")
            UserDefaults.standard.set(tanggal, forKey: "TanggalLahir")
            UserDefaults.standard.set(jeniskelamin, forKey: "kelaminUser")
            cekLogin1 = true
            
        }
        UserDefaults.standard.set(cekLogin1, forKey: "cekMasuk1")
        
        
    }
    
    //Alert
    func displayAlertMessage(userMessage: String) {
        
        var Alertnya = UIAlertController(title:"Alert", message: userMessage, preferredStyle: UIAlertController.Style.alert);
        
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
        
        Alertnya.addAction(okAction)
        
        self.present(Alertnya, animated:  true, completion: nil)
        
        
    }
    func alertSukses(title: String, message: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(ACTION) in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "IdentifyViewController") as! IdentifyViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }))
        self.present(alert, animated: true, completion: nil)
    }
   
    
    }
//DISMISS KEYBOARD
extension UIViewController {

    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}
    
    
    
    /*
    // MARK: - Navigation

     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


