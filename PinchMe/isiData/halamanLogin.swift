//
//  halamanLogin.swift
//  PinchMe
//
//  Created by Kevin Heryanto on 27/05/20.
//  Copyright © 2020 Ulinnuha Nabilah. All rights reserved.
//

import UIKit

class halamanLogin: UIViewController {

    @IBOutlet weak var inputNama: UITextField!
    @IBOutlet weak var inputTelepon: UITextField!
    var cekLogin1: Bool = Bool()
    
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        print(UserDefaults.standard.string(forKey: "namaUser"))
        print(UserDefaults.standard.string(forKey: "nomorUser"))
        SembunyiKeyboardWhenTappedAround()
        
        //set shadow for button
        loginButton.layer.shadowButton()
        signUpButton.layer.shadowButton()
    }
    
    
    @IBAction func Masuk(_ sender: UIButton) {
        if (inputNama.text == UserDefaults.standard.string(forKey: "namaUser") && inputTelepon.text == UserDefaults.standard.string(forKey: "nomorUser")) {
                   cekLogin1 = true
//                   performSegue(withIdentifier: "masukSini", sender: nil)
//                   print("halo")
            alertSukses(title: "Berhasil", message: "Anda berhasil masuk")
               }else
               {
                   displayAlertMessage(userMessage: "Data yang dimasukkan salah")
                   cekLogin1 = false
                   return;
               }
               UserDefaults.standard.set(cekLogin1, forKey: "cekMasuk1")
        }
    
    
    func displayAlertMessage(userMessage: String) {
           
           var Alertnya = UIAlertController(title:"Alert", message: userMessage, preferredStyle: UIAlertController.Style.alert);
           
           let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil)
           
           Alertnya.addAction(okAction)
           
           self.present(Alertnya, animated:  true, completion: nil)
           
           
    }
    
    func alertSukses(title: String, message: String){
               let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
               
               alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {(ACTION) in
                   let storyboard = UIStoryboard(name: "Main", bundle: nil)
                   let vc = storyboard.instantiateViewController(withIdentifier: "IdentifyViewController") as! IdentifyViewController
                   self.navigationController?.pushViewController(vc, animated: true)
               }))
               self.present(alert, animated: true, completion: nil)
           }

}
extension UIViewController {

    func SembunyiKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.hilangKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func hilangKeyboard() {
        view.endEditing(true)
    }
}
